package run;

import lib.ANN;
import lib.Perceptron;

/**
 * Created by yuri.silenok on 2/27/17.
 */
public class Program {
    public static void main(String[] args) throws Exception {
        int tableSize=10;
        int cX = tableSize*2;
        int cY = (tableSize-1)*(tableSize-1)+1;
        Perceptron  per = new Perceptron(cX,cY);
        //Perceptron  per = new Perceptron(2,1);
        int addCount=0;
        boolean complite = false;
        for (int c = 0; !complite; c++) {
            boolean trained = false;
            int t = 0;
            int tL = (addCount+1)*1000;
            for ( ;t < tL && !trained; t++) {
                trained=true;
                int countTrueObuch=0;
                int countObuch=tableSize*tableSize;

                for (int i = 0; i < tableSize; i++) {
                    for (int j = 0; j < tableSize; j++) {
                        double[] vX = new double[cX];
                        vX[i] = 1;
                        vX[tableSize + j] = 1;
                        double[] vT = new double[cY];
                        vT[i * j] = 1;

                        //vT = new double[]{i*j};

                        double[] vY = per.vectorY(vX, vT);
                        //double[] vY = per.vectorY(new double[]{i,j}, vT);
                        int indexMaxVY = 0;
                        for (int imvy = 1; imvy < vY.length; imvy++) {
                            if (vY[indexMaxVY] < vY[imvy]) {
                                indexMaxVY = imvy;
                            }
                        }

                            //if(Math.abs( vY[0] - vT[0] ) > 0.1){
                        if (indexMaxVY != i * j || vY[indexMaxVY] < 0.9) {
                            trained=false;
                        }else countTrueObuch++;
                    }
                }
                if(t%1000==0) System.out.println("Итерация " +t+ " процент "+ countTrueObuch*100/countObuch+" нейронов "+(addCount+1));


            }
            complite = trained;
            if (!trained) {
                per.AddNeuron();
                addCount++;
                System.out.println("Всего добавлено нейронов " + addCount);

            }
            else {
                System.out.println("ИНС обучена за " + t + " итераций и " + addCount + " добавленных нейронов");

            }

        }
    }
}
