package lib;

import java.lang.reflect.Array;
import java.util.Random;

/**
 * Created by yuri.silenok on 2/22/17.
 */
public class Perceptron {
    /**
     * слои ИНС
     */
    double[][][] layers;
    /**
     * Норма обучения
     */
    double q = 0.85;

    /**
     *
     * @param cX Количество входных параметров
     * @param cY Количество выходных параметров
     */
    public Perceptron(int cX, int cY) {
        layers = new double[2][][];
        layers[0] = new double[cX][1];
        initLayer(layers[0]);
        layers[1] = new double[1][cY];
        initLayer(layers[1]);
    }

    static void initLayer(double[][] layer)
    {
        int countZnach = layer.length * layer[0].length + 2;
        double  schag = 1.0/countZnach;
        for (int i = 0; i < layer.length; i++)
            for (int j = 0; j < layer[i].length; j++)
                layer[i][j] = (layer[i].length * i + j + 1) * schag;
    }

    static double Fa(double x)
    {
        //return x;
        return 1.0 / (1.0 + Math.exp(-x));
    }
    double[] vectorZ;
    public double[] vectorY(double[] vectorX){
        vectorZ = new double[this.layers[1].length];
        double[] vectorY = new double[this.layers[1][0].length];
        for (int z = 0; z < vectorZ.length; z++)
        {
            for (int x = 0; x < vectorX.length; x++)
                vectorZ[z] += vectorX[x] * this.layers[0][x][z];
            vectorZ[z] = Fa(vectorZ[z]);
        }
        for (int y = 0; y < vectorY.length; y++)
        {
            for (int z = 0; z < vectorZ.length; z++)
                vectorY[y] += vectorZ[z] * this.layers[1][z][y];
            vectorY[y] = Fa(vectorY[y]);
        }
        return vectorY;
    }
    public double[] vectorY(double[] vectorX, double[] vectorT)
    {
        //опрос сети
        double[] vectorY = vectorY(vectorX);
       //погрешность
        double[] qY = new double[vectorY.length];
        for (int y = 0; y < qY.length; y++)
            qY[y] = (vectorT[y] - vectorY[y]) * vectorY[y] * (1 - vectorY[y]);
        double[] qZ = new double[vectorZ.length];
        for (int z = 0; z < qZ.length; z++)
        {
            for (int y = 0; y < vectorY.length; y++)
                qZ[z] += qY[y] * this.layers[1][z][y];
            qZ[z] *= vectorZ[z] * (1 - vectorZ[z]);
        }
        for (int x = 0; x < vectorX.length; x++)
            for (int z = 0; z < vectorZ.length; z++)
                this.layers[0][x][ z] += vectorX[x] * qZ[z] * q;
        for (int z = 0; z < vectorZ.length; z++)
            for (int y = 0; y < vectorY.length; y++)
                this.layers[1][z][y] += vectorZ[z] * qY[y] * q;
        return vectorY;
    }

    public void AddNeuron(){
        double[][] tmpLayer = this.layers[0];
        this.layers[0] = new double[tmpLayer.length][tmpLayer[0].length+1];
        for (int i = 0; i < tmpLayer.length; i++) {
            for (int j = 0; j < tmpLayer[i].length; j++) {
                this.layers[0][i][j] = tmpLayer[i][j];
            }
            this.layers[0][i][tmpLayer[i].length]=0.5;
        }

        tmpLayer = this.layers[1];
        this.layers[1] = new double[tmpLayer.length+1][tmpLayer[0].length];
        for (int i = 0; i < tmpLayer.length; i++) {
            for (int j = 0; j < tmpLayer[i].length; j++) {
                this.layers[1][i][j] = tmpLayer[i][j];
            }
        }
        for (int j = 0; j < tmpLayer[0].length; j++) {
            this.layers[1][this.layers[1].length-1][j] = 0.5;
        }
    }
}
